class ApplicationController < ActionController::API
	before_action :authenticate_and_set_user
	
	def data
		extra_info = { insurance_number:  SecureRandom.urlsafe_base64(10), 
					  insurance_amount: [rand(500000..600000), " INR"].join,
					   insurance_claim_status: current_user.id.even? ? "UNCLAIMED" : "CLAIMED"
					 }
		render json: {basic_info: current_user, confidential: extra_info}
	end

	def update_username
		if User.find(current_user.id).update(name: params[:name])
			render json: {msg: 'updated successfully', basic_info: User.find(current_user.id)}
		else
			render json: {msg: "error occurred. Please try again later"}
		end
	end
end
