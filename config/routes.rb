Rails.application.routes.draw do
	api_guard_routes for: 'users'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/info', to: "application#data"
  post '/update', to: "application#update_username"
end
